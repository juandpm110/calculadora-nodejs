//importar la clase express
import express from "express";
import agregar_sitio from './agregar_sitio.js';
import consultar_sitio from './consultar_sitio.js';

//crear un objeto express
const app=express();
const puerto=3003;

//crear una ruta
 app.get("/", function(req,res){
     res.send("bienvenido al mundo del Backend");
 });

 app.get("/sumar", function(req,res){
    
    let resultado=Number(req.query.a)+Number(req.query.b);
    
    res.send(resultado.toString());
});

app.get("/restar", function(req,res){
    
    let resultado=Number(req.query.a)-Number(req.query.b);
    
    res.send(resultado.toString());
});

app.get("/multiplicar", function(req,res){
    
    let resultado=Number(req.query.a)*Number(req.query.b);
    
    res.send(resultado.toString());
});

app.get("/dividir", function(req,res){
    
    if(Number(req.query.b)==0){
        res.send("No se puede dividir por cero")
    }else{
        let resultado=Number(req.query.a)/Number(req.query.b);
        res.send(resultado.toString());
    }
    
});

app.get("/modulo", function(req,res){
    
    let resultado=Number(req.query.a)%Number(req.query.b);
    
    res.send(resultado.toString());
});


app.get("/agregar_sitio", function(req,res){
    res.send(agregar_sitio());
});

app.get("/consultar_sitio", function(req,res){
    res.send(consultar_sitio(req));
});


 //inicializar el server
 app.listen(puerto, ()=>{
    console.log("está funcionando el server");
 });