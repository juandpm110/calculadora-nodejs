import { json } from 'express';
import mysql from 'mysql';

export default function consultar_sitio(req){
    let conexion;
    let jsongobal;
    
    //Definir lo parámetros de conexión a MySQL
    let parametros={
        host:'localhost',
        user: 'juan',
        password: 'password',
        database: 'wallettrip_db'
    }
    //crear conexión a una base de datos
    conexion=mysql.createConnection(parametros);

    //Conectarnos al servidor MySQL

    conexion.connect(function (err){
        if(err){
            console.log("error" + err.message);
            return false;
        }else{
            console.log("conectado al servidor MySQL")
            return true;
        }
    });


    //Realizar la consulta SQL

    let valor=req.query.a;
    // me puedo ahorrar esta linea de codigo escribiendo en el array req.query.a

    let consulta = `SELECT idplace, name, description, country_code, region, location FROM place WHERE idplace=?;`;


    conexion.query(consulta, [valor], (err, results, fields)=>{
        if(err){
            console.error("error" + err.message)
        }else{
            console.log(results);
            let mijson = JSON.stringify(results);
            console.log(mijson);
            mijson = jsongobal;
        }
    });

    //Desconectarnos del servidor MySQL
    conexion.end();
    return jsongobal;
}