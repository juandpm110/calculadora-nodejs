import mysql from 'mysql';

export default function agregar_sitio(){
    let conexion;
    
    //Definir lo parámetros de conexión a MySQL
    let parametros={
        host:'localhost',
        user: 'juan',
        password: 'password',
        database: 'wallettrip_db'
    }
    //crear conexión a una base de datos
    conexion=mysql.createConnection(parametros);

    //Conectarnos al servidor MySQL

    conexion.connect(function (err){
        if(err){
            console.log("error" + err.message);
            return false;
        }else{
            console.log("conectado al servidor MySQL")
            return true;
        }
    });



    //Realizar la consulta SQL


    let consulta = `INSERT INTO place (idPlace, name, description, country_code, region, location) VALUES (3, 
        "Universidad Distrital", 
        "universidad del distrito de bogotá", 
        "CO", 
        "Bogotá D.C.", 
        ST_GeomFromText("POINT(4.610903352055752 -74.07034308190232)"))`;
    conexion.query(consulta);

    //Desconectarnos del servidor MySQL

    conexion.end();
    return "Insertado";
}